#ifndef DECORATION_DECORATION_HPP
#define DECORATION_DECORATION_HPP

#include <map>
#include <memory>
#include <typeinfo>
#include <typeindex>
#include <type_traits>

namespace decoration
{
   class decorator
   {      
   public:
      decorator() = default;
      virtual ~decorator() = default;
      
      decorator(decorator&&) = default;
      decorator& operator=(decorator&&) = default;
      
      decorator(const decorator&) = default;
      decorator& operator=(const decorator&) = default;
   };
   
   class decorator_holder
   {
   public:
      decorator_holder() = default;
      virtual ~decorator_holder() = default;
      
      decorator_holder(decorator_holder&&) = default;
      decorator_holder& operator=(decorator_holder&&) = default;
      
      decorator_holder(const decorator_holder&) = delete;
      decorator_holder& operator=(const decorator_holder&) = delete;
      
      /**
       * Add a Decorator via its constructor arguments
       * @param[in] args Constructor arguments for Decorator
       * @return false if Decorator could not be added, or is already added
       **/
      template<class Decorator, class... Args>
      bool emplace_decorator(Args&&... args)
      {
         static_assert(std::is_base_of<decorator, Decorator>());
         return m_decorators.emplace(typeid(Decorator), std::make_unique<Decorator>(std::forward<Args>(args)...)).second;
      }
      
      /**
       * Add a Decorator via its default constructor 
       * @return false if Decorator could not be added, or is already added
       **/
      template<class Decorator>
      bool emplace_decorator()
      {
         static_assert(std::is_base_of<decorator, Decorator>());
         return m_decorators.emplace(typeid(Decorator), std::make_unique<Decorator>()).second;
      }
      
      /**
       * Determine if the Decorator has been added
       **/
      template<class Decorator>
      bool has_decorator() const
      {
         static_assert(std::is_base_of<decorator, Decorator>());
         return m_decorators.find(typeid(Decorator)) != m_decorators.end();
      }
      
      /**
       * @throw std::out_of_range if Decorator has not been added
       * @throw std::bad_cast is an internal error
       **/
      template<class Decorator>
      Decorator& get_decorator()
      {
         static_assert(std::is_base_of<decorator, Decorator>());
         return dynamic_cast<Decorator&>(*m_decorators.at(typeid(Decorator)).get());
      }
      
      /**]=
       * @throw std::out_of_range if Decorator has not been added
       * @throw std::bad_cast is an internal error
       **/
      template<class Decorator>
      const Decorator& get_decorator() const
      {
         static_assert(std::is_base_of<decorator, Decorator>());
         return dynamic_cast<Decorator&>(*m_decorators.at(typeid(Decorator)).get());
      }
   private:
      std::map<std::type_index, std::unique_ptr<decorator>> m_decorators;
   };
}

#endif
