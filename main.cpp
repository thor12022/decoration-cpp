#include <iostream>

#include "Decoration.hpp"
#include <random>
#include <string>

class decorator_float : public decoration::decorator
{
public:
   decorator_float(float f) : value{ f }
   {}
   
   virtual ~decorator_float() = default;
   
   float value;
};

class decorator_int : public decoration::decorator
{
public:
   decorator_int(int f) : value{ f }
   {}
   
   virtual ~decorator_int() = default;
   
   int value;
};

class decorator_rand : public decoration::decorator
{
public:
   decorator_rand()
   {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_int_distribution<> dis(0, 9999);
      value = dis(gen);
   }
   
   virtual ~decorator_rand() = default;
   
   float value;
};

class decorator_string : public decoration::decorator
{
public:
   decorator_string(std::string str) : value{ std::move(str) }
   {}
   
   virtual ~decorator_string() = default;
   
   std::string value;
};

class decorator_unused : public decoration::decorator
{
public:
   decorator_unused()
   {}
   
   virtual ~decorator_unused() = default;
};

class test_holder : public decoration::decorator_holder
{
public:
   using decorator_holder::decorator_holder;
};

int main(int, char **)
{
   test_holder holder;
   
   holder.emplace_decorator<decorator_float>( 2.5f );
   holder.emplace_decorator<decorator_int>( 8675309 );
   holder.emplace_decorator<decorator_rand>();
   holder.emplace_decorator<decorator_string>("String Test");
   
   if(holder.has_decorator<decorator_float>())
   {
      std::cout << "decorator_float value: " << holder.get_decorator<decorator_float>().value++ << std::endl;
   }
   
   if(holder.has_decorator<decorator_float>())
   {
      std::cout << "modified decorator_float value: " << holder.get_decorator<decorator_float>().value++ << std::endl;
   }
   
   if(holder.has_decorator<decorator_int>())
   {
      std::cout << "decorator_int value: " << holder.get_decorator<decorator_int>().value << std::endl;
   }
   
   if(holder.has_decorator<decorator_rand>())
   {
      std::cout << "decorator_rand value: " << holder.get_decorator<decorator_rand>().value << std::endl;
   }
   
   if(holder.has_decorator<decorator_string>())
   {
      std::cout << "decorator_string value: " << holder.get_decorator<decorator_string>().value << std::endl;
   }   
   
   if(!holder.has_decorator<decorator_unused>())
   {
      std::cout << "decorator_unused not present\n";
   }
   
   return 0;
}
